/*
Tests that the implementation correctly takes the least upper-bound of both
branches, as "a" is only initialized in one branch.
*/

class Main {
    public static void main(String[] args) {
        A a;
        Builder b;
        int x;

        b = new Builder();
        if (true) {
            a = b.createA();
        } else {
            a = b.getNullA();
        }

        x = a.m();
        System.out.println(x);
    }
}

class A {
    public int m() {
        return 1;
    }
}

class Builder {
    A field;

    public A createA() {
        return new A();
    }

    public A getNullA() {
        return field;
    }
}
